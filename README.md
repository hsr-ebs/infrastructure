# Infrastructure

This repository contains all kubernetes configurations.

## Template chart 
```
task template
```

## Deploy chart
```
task deploy
```

## Requirements
- kubectl (https://kubernetes.io/de/docs/tasks/tools/install-kubectl)
- Helm (https://helm.sh/)
- Task (https://taskfile.dev)